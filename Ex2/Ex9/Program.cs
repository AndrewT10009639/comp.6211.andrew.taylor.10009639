﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex9
{
    class Program
    {
        static void Main(string[] args)
        {
           int a = Convert.ToInt32(Console.ReadLine());
           int b = Convert.ToInt32(Console.ReadLine());
           int c = Convert.ToInt32(Console.ReadLine());
           int[] numbers = { a, b, c};
           int max = numbers.Max();
           Console.WriteLine(max);
        } 
    }
}
