﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex8
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = Convert.ToInt32(Console.ReadLine());
            if (num < 0)
            {
                Console.WriteLine("number is negative");
                Console.ReadLine();
            }
            if (num > 0)
            {
                Console.WriteLine("Number is positive");
                Console.ReadLine();
            }
            if (num == 0)
            {
                Console.WriteLine("the number is zero");
                Console.ReadLine();
            }
        }
    }
}
